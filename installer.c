/*
Copyright (C) 2013, 2014, 2015, 2016 Digital Freedom Foundation
This file is part of GNUKhata:A modular,robust and Free Accounting System.

GNUKhata is Free Software; you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version.and old.stockflag = 's'
GNUKhata is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public
License along with GNUKhata (COPYING); if not, write to the
Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301  USA59 Temple Place, Suite 330,


Contributor:
"Abhijith Balan" <abhijithb21@openmailbox.org>
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

static void
gk_install (GtkWidget *widget,
             gpointer   data)
{
  system ("chmod 775 setup.sh");
  system ("x-terminal-emulator -e sudo ./setup.sh");
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *button_box;
  GtkWidget *dialog;
  GtkWidget *label;
  GtkWidget *label1;
  GtkWidget *content_area;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "GNUKhata Installer");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);
  
  button_box = gtk_button_box_new (GTK_ORIENTATION_VERTICAL);
  gtk_container_add (GTK_CONTAINER (window), button_box);
  label = gtk_label_new( "\n\n\tYou are about to install GNUKhata - a free and opensource accounting software.\t\n\n\tBefore installing GNUKhata you may read the software license in COPYING file.\t\n\tGNUKhata is licensed under GNU Affero GPL V3.\t\n\tClick the 'Proceed' button to start installation.\t\n\n" );
  button = gtk_button_new_with_label ("Proceed");
  label1 = gtk_label_new( "\n" );
  g_signal_connect (button, "clicked", G_CALLBACK (gk_install), NULL);
  g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
  gtk_container_add (GTK_CONTAINER (button_box), label);
  gtk_container_add (GTK_CONTAINER (button_box), button);
  gtk_container_add (GTK_CONTAINER (button_box), label1);

  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
