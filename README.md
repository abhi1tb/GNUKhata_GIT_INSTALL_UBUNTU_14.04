# Copyright (C) 2013, 2014, 2015, 2016 Digital Freedom Foundation
#  This file is part of GNUKhata:A modular,robust and Free Accounting System.
#  You can copy or modify it under the terms of GNU Free Documentation Licence (GFDL) 
#  version 3 or any later version.

#  These instructions are provided as is without any worenty even without the implied 
#  warranty of merchantibility or fitness for a certain purpose. Digital Freedom Foundation 
#  or the author of this document will not be liable for any damage caused by the use of this 
#  document, including but not limited to insidental, accidental, consequential or punitive damages.

#  GNUKhata is Free Software; you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.

#  GNUKhata is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#  You should have received a copy of the GNU Affero General Public
#  License along with GNUKhata (COPYING); if not, write to the
#  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA  02110-1301  USA59 Temple Place, Suite 330,


# Author:
# "Abhijith Balan" <abhijithb21@disroot.org>
 
This is an installer for GNUKhata that downloads files directly from GIT. This installer needs 
internet for installing GNUKhata and works on Debian based distros. Double click the "installer" file 
to start the installation.

Clicking the "Proceed" button opens up a terminal window. It asks for your 
system password. Enter the password and press "Enter" key. This triggers 
a series of commands. Sit back and relax. The terminal window will close 
automatically after the installation is complete.

If you are using a desktop environment like KDE, XFCE, LXDE or MATE chances 
are the terminal window does not appear. In that case you can run the 
"setup.sh" file manually from your terminal. Change directory to
the one containing the file and enter the following commands.

chmod 755 setup.sh
sudo ./setup.sh
